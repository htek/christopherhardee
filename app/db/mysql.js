var Sequelize = require('sequelize');
var User = require('./models/User');
var Blogpost = require('./models/Blogpost');

var sequelize = new Sequelize('christopherhardee', 'root', 'reality', {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },

    // SQLite only
    // storage: 'path/to/database.sqlite',

    define: {
        timestamps: true // true by default
    }
});

var user = sequelize.define('user', User);
var blogpost = sequelize.define('blogpost', Blogpost);

module.exports = {
    db: sequelize,
    Users: user,
    Blogposts: blogpost 
};