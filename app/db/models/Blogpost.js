var Sequelize = require('sequelize');
var Post = {
     post_title:{
        type: Sequelize.STRING
     },
     post_type: {
         type: Sequelize.STRING
     },
     images: {
         type: Sequelize.STRING
     },
     post_body: {
         type: Sequelize.STRING
     }
};


module.exports = Post;