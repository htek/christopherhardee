var Sequelize = require('sequelize');
var User = {
     name:{
        type: Sequelize.STRING
    },
     password: {
        type: Sequelize.STRING
     },
     email: {
         type: Sequelize.STRING
     },
     role: {
         type: Sequelize.STRING
     }
     
};


module.exports = User;