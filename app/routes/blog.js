var express = require('express');
var router = express.Router();

var blogrepo = require('../repositories/blog-list');

router.get('/', function(req, res, next) {
    debugger;
    blogrepo.read(['id', 'post_title', 'post_body', 'createdAt'],function(items) {
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            item.post_body = item.post_body.substring(0,200);
        }
        res.render('blog-list', { list: items });
    });
});


module.exports = router;
