var express = require('express');
var router = express.Router();

var userrepo = require('../repositories/user');


/* GET users listing. */

router.get('/', function(req, res, next) {
  userrepo.read(['email','role'], function(users) {
    //res.send(JSON.stringify(email, null, 4));
    res.render('users', { users: users });
  });
});


router.get('/create', function(req, res, next) {
  res.render('users/create', { title: 'Get Contact' });
});

router.post('/create', function(req, res, next) {
  console.log(req.body.name);
  userrepo.create(req.body.name, req.body.password, req.body.email, req.body.role);
  res.render('users/create', { title: 'Post Created Contacts' });
});
module.exports = router;
