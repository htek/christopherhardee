var express = require('express');
var router = express.Router();

var blogrepo = require('../repositories/blog-post');


router.get('/id/:id', function(req, res, next) {
    console.log('asd')
    var id = req.params['id'];
    if (id > 0) {     
            blogrepo.read(id, function(items) {
            console.dir(items)
            res.render('blog-post', { list: items, id: id });
        }); 
    } else {
            blogrepo.read(0, function(items) {
            res.render('blog-post', { list: items });
        });            
    }
    console.log('asd')    
});

router.get('/create', function(req, res, next){
  res.render('blog/create', { title: 'Blog Post'})
});

router.post('/create', function(req, res, next) {
  console.log(req.body.name);
  blogrepo.create(req.body.post_title, req.body.post_type, req.body.images, req.body.post_body);
  res.render('blog/create', { title: 'Post Created Contacts' });
});

module.exports = router;
