module.exports =  function(passport) {
  var express = require('express');
  var router = express.Router();
  
  /* GET home page. */
  router.get('/', function(req, res, next) {
    res.render('login', { title: 'Express' });
  });
  
  // router.post('/', function(req, res, next){
  //   res.render('index', { title: 'Users List'});
  // });
  
  router.post('/',
  passport.authenticate('local', { successRedirect: '/', failureRedirect: '/login' }), function(req, res, next) {
    res.render('login', { title: 'Express' });
  });
  return router;
} 
