var context = require('../db/mysql');

function create(post_title, post_type, images, post_body){
    context.Blogposts.create({
        post_title: post_title,
        post_type: post_type,
        images: images,
        post_body: post_body
    })
}

function read(id, callback){
    //sequelize > getbyid
    context.Blogposts.findById(id,
    {
        attributes: ['id','post_title', 'post_type', 'post_body', 'createdAt']
    }).then(function(items) {
        callback(items);
    })
}

function update(){
    context.Blogpost.update({
        post_title: post_title,
        post_type: post_type,
        images: images,
        post_body: post_body
    })
}

function remove(){
    context.Blogpost.remove({
        post_title: post_title,
        post_type: post_type,
        images: images,
        post_body: post_body
    })
}

var repo = {
    create: create, 
    read:  function(id, callback) {
        read(id, callback)
    }, 
    update: update, 
    delete: remove
};

module.exports = repo;