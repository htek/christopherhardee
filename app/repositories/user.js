var context = require('../db/mysql');

function create(name, password, email, role){
    context.Users.create({
        name: name,
        password: password,
        email: email,
        role: role
    })
}

function ORMread(attributes, callback){
    context.Users.findAll({
        attributes: attributes
    }).then(function(users) {
        callback(users);
    })
}

function update(){
    context.Users.update({
        name: name,
        password: password,
        email: email,
        role: role
    })
}

function getUserByEmail(email, callback){
    context.Users.findOne({
        email: email
    }).then((user)=>{
        callback(false, user);
    });
}

function remove(){
    context.Users.remove({
        name: name,
        password: password,
        email: email,
        role: role
    })
}

var repo = {
    create: create, 
    read:  function(attributes, callback) {
        ORMread(attributes, callback)
    }, 
    getUserByEmail,
    update: update, 
    delete: remove
};

module.exports = repo;