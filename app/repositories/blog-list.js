var context = require('../db/mysql');

function ORMread(attributes, callback) {
    context.Blogposts.findAll({
        attributes: attributes
    }).then(function (items) {
        cutBody(items);
        callback(items);
    })
}

var repo = {
    read: function (attributes, callback) {
        ORMread(attributes, callback)
    },
};

function cutBody(items) {
    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        item.post_body = item.post_body.substring(0, 200);
    }
}

module.exports = repo;