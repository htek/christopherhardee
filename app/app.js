var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport')
, LocalStrategy = require('passport-local').Strategy;

var User = {
  findOne: function(username) {
    return {
      err: null, user: { name: '123' }
    }
  }
}

passport.use(new LocalStrategy(function(username, password, done) {
    User.findOne({ username: username }, function(err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, user);
    });
  }
));

//db
var context = require('./db/mysql');

context.db.sync({
  force: true
}).then(() => {
  context.Users.create({
    name: "Chris Hardee",
    password: "test",
    email: "chris@mousebit.com",
    role: "admin"
  });
}).then(() => {
  context.Blogposts.create({
    post_title: "test",
    post_type: "programming",
    images: "none",
    post_body: "This is a test to see if my code works"
  });
}).then(() => {
  context.Users.create({
    name: "John Test",
    password: "test",
    email: "test@test.com",
    role: "member"
  });
});


//routes
var index = require('./routes/index');
var users = require('./routes/users');
var about = require('./routes/about');
var portfolio = require('./routes/portfolio');
var blog = require('./routes/blog');
var services = require('./routes/services');
var portfolioitem = require('./routes/portfolioitem');
var contact = require('./routes/contact');
var blogpost = require('./routes/blog-post');
var blog = require('./routes/blog');
var login = require('./routes/login')(passport);
var admin = require('./routes/admin');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// For Passport
app.use(session({
  secret: 'keyboard cat',
  resave: true,
  saveUninitialized: true
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

//routes (controllers)
app.use('/', index);
app.use('/users', users);
app.use('/about', about);
app.use('/portfolio', portfolio);
app.use('/blog', blog);
app.use('/services', services);
app.use('/portfolio-item', portfolioitem);
app.use('/contact', contact);
app.use('/blog-post', blogpost);
app.use('/login', login);
app.use('/admin', admin);

require('express-debug')(app, {});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
